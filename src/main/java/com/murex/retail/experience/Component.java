package com.murex.retail.experience;

public class Component {
    private String id;
    private String category;
    private String name;
    private String brand;
    private String productLine;
    private String numberOfCores;
    private String processorClockSpeed;
    private String graphicClockSpeed;
    private String dimension;
    private String resolution;
    private String color;
    private String componentInterface;
    private String size;
    private int price;
    private int quantity;

    protected Component(ComponentBuilder builder) {
        this.id = builder.id;
        this.category = builder.category;
        this.name = builder.name;
        this.brand = builder.brand;
        this.productLine = builder.productLine;
        this.numberOfCores = builder.numberOfCores;
        this.processorClockSpeed = builder.processorClockSpeed;
        this.graphicClockSpeed = builder.graphicClockSpeed;
        this.dimension = builder.dimension;
        this.resolution = builder.resolution;
        this.color = builder.color;
        this.componentInterface = builder.componentInterface;
        this.size = builder.size;
        this.price = builder.price;
        this.quantity = builder.quantity;
    }

    public String getId() {
        return this.id;
    }

    public String getCategory() {
        return this.category;
    }

    public String getName() {
        return this.name;
    }

    public String getBrand() {
        return this.brand;
    }

    public String getProductLine() {
        return this.productLine;
    }

    public String getNumberOfCores() {
        return this.numberOfCores;
    }

    public String getProcessorClockSpeed() {
        return this.processorClockSpeed;
    }

    public String getDimension() {
        return this.dimension;
    }

    public String getResolution() {
        return this.resolution;
    }

    public String getColor() {
        return this.color;
    }

    public String getComponentInterface() {
        return this.componentInterface;
    }

    public String getSize() {
        return this.size;
    }

    public int getPrice() {
        return this.price;
    }

    public int getQuantity() {
        return this.quantity;
    }
    public String print() {
        return this.id+"\t|\t"+this.category+"\t|\t"+this.name+"\t|\t"+this.brand+"\t|\t"+this.productLine+"\t|\t"+this.numberOfCores
                +"\t|\t"+this.processorClockSpeed+"\t|\t"+this.graphicClockSpeed+"\t|\t"+this.dimension+"\t|\t"+this.resolution+"\t|\t"+this.color
                +"\t|\t"+this.componentInterface+"\t|\t"+this.size+"\t|\t"+this.price+"\t|\t"+this.quantity;
    }

}
