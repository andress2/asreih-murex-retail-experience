package com.murex.retail.experience;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class Functionalities {
    private static final Logger logger = LogManager.getLogger(Functionalities.class);

    public static void sortByCategoryNameBrand(List<Component> computerList) {
        computerList.stream().sorted(Comparator.comparing(Component::getCategory)
                .thenComparing(Component::getName)
                .thenComparing(Component::getBrand))
                .limit(10)
                .forEach((computer) -> logger.info(computer.getId() + "\t|\t" + computer.getCategory() + "\t|\t" + computer.getName() + "\t|\t" + computer.getBrand()));
    }

  /*  public static void sortByName(List<Component> computerList) {
        computerList.stream().sorted(Comparator.comparing(Component::getName))
                .limit(10)
                .forEach((computer) -> logger.info(computer.getId() + "\t|\t" + computer.getCategory() + "\t|\t" + computer.getName() + "\t|\t" + computer.getBrand()));

    }

    public static void sortByBrand(List<Component> computerList) {
        computerList.stream().sorted(Comparator.comparing(Component::getBrand))
                .limit(10)
                .forEach((computer) -> logger.info(computer.getId() + "\t|\t" + computer.getCategory() + "\t|\t" + computer.getName() + "\t|\t" + computer.getBrand()));

    }*/

    public static void averagePrice(List<Component> computerList) {
        logger.info((computerList.stream()
                .mapToDouble(Component::getPrice)
                .sum()) / computerList.size());
    }

    public static void averagePriceOfTheCPU(List<Component> computerList) {
        logger.info((computerList.stream().filter(o -> o.getCategory().equals("CPU"))
                .mapToDouble(Component::getPrice)
                .sum())
                / (computerList.stream()
                .filter(o -> o.getCategory().equals("CPU"))
                .count()));
    }

    public static void cheapestComponent(List<Component> computerList) {
        logger.info(computerList.stream()
                .min(Comparator.comparingInt(Component::getPrice))
                .get().print());
    }

    public static void mostExpensiveComponents(List<Component> computerList) {
        logger.info(computerList.stream()
                .max(Comparator.comparingInt(Component::getPrice))
                .get().print());
    }

    public static void groupedCategory(List<Component> computerList) {
        logger.info(computerList.stream()
                .collect(groupingBy(p -> p.getCategory(), Collectors.counting())));
    }

    public static void groupedCategoryBrand(List<Component> computerList) {
        logger.info(computerList.stream()
                .collect(Collectors.groupingBy(x -> new ArrayList<String>(Arrays.asList(x.getCategory(), x.getBrand())),
                Collectors.counting())));
    }

    public static void groupedCategoryBrandSumOfQuantity(List<Component> computerList) {
        logger.info(computerList.stream()
                .collect(Collectors.groupingBy(x -> new ArrayList<String>(Arrays.asList(x.getCategory(), x.getBrand())),
                        Collectors.summingInt(Component::getQuantity))));
    }
}
