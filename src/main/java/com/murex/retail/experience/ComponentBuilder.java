package com.murex.retail.experience;

public class ComponentBuilder { //inconvenient ComponentBuilder.id
    protected String id;
    protected String category;
    protected String name;
    protected String brand;
    protected String productLine;
    protected String numberOfCores;
    protected String processorClockSpeed;
    protected String graphicClockSpeed;
    protected String dimension;
    protected String resolution;
    protected String color;
    protected String componentInterface;
    protected String size;
    protected int price;
    protected int quantity;
    //TODO: move to a separate class
    public ComponentBuilder() {

    }

    public Component build() {
        return new Component(this);
    }

    public ComponentBuilder id(String id) {
        if (isValidString(id)) {
            this.id = id;
        }
        return this;
    }

    public ComponentBuilder category(String category) {
        //TODO: create a helper method -- isValidString
        if (isValidString(category)) {
            this.category = category;
        }
        return this;
    }

    public ComponentBuilder name(String name) {
        if (isValidString(name)) {
            this.name = name;
        }
        return this;
    }

    public ComponentBuilder brand(String brand) {
        if (isValidString(brand)) {
            this.brand = brand;
        }
        return this;
    }

    public ComponentBuilder productLine(String productLine) {
        if (isValidString(productLine)) {
            this.productLine = productLine;
        }
        return this;
    }

    public ComponentBuilder numberOfCores(String numberOfCores) {
        if (isValidString(numberOfCores)) {
            this.numberOfCores = numberOfCores;
        }
        return this;
    }

    public ComponentBuilder processorClockSpeed(String processorClockSpeed) {
        if (isValidString(processorClockSpeed)) {
            this.processorClockSpeed = processorClockSpeed;
        }
        return this;
    }

    public ComponentBuilder graphicClockSpeed(String graphicClockSpeed) {
        if (isValidString(graphicClockSpeed)) {
            this.graphicClockSpeed = graphicClockSpeed;
        }
        return this;
    }

    public ComponentBuilder dimension(String dimension) {
        if (isValidString(dimension)) {
            this.dimension = dimension;
        }
        return this;
    }

    public ComponentBuilder resolution(String resolution) {
        if (isValidString(resolution)) {
            this.resolution = resolution;
        }
        return this;
    }

    public ComponentBuilder color(String color) {
        if (isValidString(color)) {
            this.color = color;
        }
        return this;
    }

    public ComponentBuilder Interface(String componentInterface) {
        if (isValidString(componentInterface)) {
            this.componentInterface = componentInterface;
        }
        return this;
    }

    public ComponentBuilder size(String size) {
        if (isValidString(size)) {
            this.size = size;
        }
        return this;
    }

    public ComponentBuilder price(String price) {
        if (isValidString(price)) {
            this.price = Integer.parseInt(price);
        }
        return this;
    }

    public ComponentBuilder quantity(String quantity) {
        if (isValidString(quantity)) {
            this.quantity = Integer.parseInt(quantity);
        }
        return this;
    }

    private boolean isValidString(String Field)
    {
        return !Field.equals("N/A");
    }
}