package com.murex.retail.experience;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);


    public static void main(String[] args) {

//          System.out.println(createMessage());
        try {
            readData();
//          readThemAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void readData() throws IOException {
        try (BufferedReader br = Files.newBufferedReader(Paths.get("C:\\code\\exercise\\andresreih_repo\\src\\main\\resources\\Inventory.csv"))) {
            ComponentBuilder builder = new ComponentBuilder();
            List<Component> computerList = new ArrayList<>();
            List<String> trimmedValues;
            String inValue = br.readLine();
            while ((inValue = br.readLine()) != null) {
                String[] componentList = inValue.split(",");
                //TODO: use trimmed values
                trimmedValues = Arrays.stream(componentList)
                        .map(componentAttribute -> componentAttribute.trim())
                        .collect(Collectors.toList());

                builder.id(trimmedValues.get(0)).category(trimmedValues.get(1)).name(trimmedValues.get(2)).brand(trimmedValues.get(3)).productLine(trimmedValues.get(4))
                        .numberOfCores(trimmedValues.get(5)).processorClockSpeed(trimmedValues.get(6)).graphicClockSpeed(trimmedValues.get(7))
                        .dimension(trimmedValues.get(8)).resolution(trimmedValues.get(9)).color(trimmedValues.get(10)).Interface(trimmedValues.get(11))
                        .size(trimmedValues.get(12)).price(trimmedValues.get(13)).quantity(trimmedValues.get(14));
                computerList.add(builder.build());
            }
            Functionalities functions = new Functionalities();
            //TODO: use lambda
            functions.sortByCategoryNameBrand(computerList);
            logger.info("\n");
//            functions.sortByName(computerList);
//            logger.info("\n");
//            functions.sortByBrand(computerList);
//            logger.info("\n");
            functions.averagePrice(computerList);
            logger.info("\n");
            functions.averagePriceOfTheCPU(computerList);
            logger.info("\n");
            functions.cheapestComponent(computerList);
            logger.info("\n");
            functions.mostExpensiveComponents(computerList);
            logger.info("\n");
            functions.groupedCategory(computerList);
            logger.info("\n");
            functions.groupedCategoryBrand(computerList);
            logger.info("\n");
            functions.groupedCategoryBrandSumOfQuantity(computerList);
        }

    }


}